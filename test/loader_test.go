package test

import (
	"testing"

	"gitlab.com/republico/util/dep_struts/internal/app/loader"
)

func TestLoad(t *testing.T) {
	loader.Config = loader.ConfigStruct{
		PathFileConfig: "C:/Teste/republico/dep_struts/loader/Gopkg.toml",
		PathLoad: "C:/republico/dep_struts/loader",
		Git: true,
		Update: true,
	}

	err := loader.Load()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}