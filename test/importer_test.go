package test

import (
	"testing"

	"gitlab.com/republico/util/dep_struts/internal/app/importer"
)

func TestImport(t *testing.T) {
	importer.Config = importer.ConfigStruct{
		PathProject: "C:/Teste/republico/dep_struts/importer",
		Git: true,
	}

	err := importer.Import()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}