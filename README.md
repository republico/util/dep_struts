# Dep Struts

## Arquivos

| Descrição | Link |
| --------- | ---- |
| Executável importer da última versão para Windows | [importer.exe](/uploads/8cb3ba22145b068428fcbdc99045b04e/importer.exe) |
| Executável importer da última versão para Linux | [importer](/uploads/b1991e00083a823a5080ebded4680eb0/importer) |
| Executável loader da última versão para Windows | [loader.exe](/uploads/abb707473234748f8ef12173b999f56b/loader.exe) |
| Executável loader da última versão para Linux | [loader](/uploads/6b5d8d2b19aa6c114b300e94a3645435/loader) |
| Arquivo de configuração para o carregamento de todos os projetos do Republico | [Gopkg.toml](https://gitlab.com/republico/doc/project/raw/master/documentation/Gopkg.toml?inline=false) |

## Depuração

```
go run cmd/importer/main.go -path C:/Teste/republico/dep_struts/importer -git true

go run cmd/loader/main.go -file C:/Teste/republico/dep_struts/loader/Gopkg.toml -path C:/Teste/republico/dep_struts/loader -git true -update true
```

## Testes

### Testes unitários

```
go test ./test -v -run TestImport

go test ./test -v -run TestLoad
```

### Testes do executável

```
bin/importer.exe -path C:/Teste/republico/dep_struts/importer -git true

bin/loader.exe -file C:/Teste/republico/dep_struts/loader/Gopkg.toml -path C:/Teste/republico/dep_struts/loader -git true -update true
```

## Build

Com debug ativado:
```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/importer.exe cmd/importer/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/importer cmd/importer/*.go
GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/importer cmd/importer/*.go
GOOS=freebsd GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/importer cmd/importer/*.go

GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/loader.exe cmd/loader/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/loader cmd/loader/*.go
GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/loader cmd/loader/*.go
GOOS=freebsd GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/loader cmd/loader/*.go
```

Com debug desativado:
```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/importer.exe cmd/importer/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/importer cmd/importer/*.go
GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/importer cmd/importer/*.go
GOOS=freebsd GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/importer cmd/importer/*.go

GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/loader.exe cmd/loader/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/loader cmd/loader/*.go
GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/loader cmd/loader/*.go
GOOS=freebsd GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/loader cmd/loader/*.go
```