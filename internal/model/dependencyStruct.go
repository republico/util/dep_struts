package model

type DependencyStruct struct {
	Name string `toml:"name"`
	Version string `toml:"version"`
	Branch string `toml:"branch"`
	Scope string `toml:"scope"`
	Url string
	Path string
}

type DependenciesStruct struct {
	Dependencies []DependencyStruct `toml:"constraint"`
}