package loader

type ConfigStruct struct {
	PathFileConfig string
	PathLoad string
	Git bool
	Update bool
}

var Config ConfigStruct
