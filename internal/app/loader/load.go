package loader

import (
	"fmt"
	"os"
	"strings"

	"github.com/BurntSushi/toml"

	"gitlab.com/republico/library/go/util/pkg/file"
	"gitlab.com/republico/library/go/util/pkg/system"

	"gitlab.com/republico/library/go/util/pkg/exception"
	"gitlab.com/republico/util/dep_struts/internal/app/importer"
	"gitlab.com/republico/util/dep_struts/internal/model"
	"gitlab.com/republico/util/dep_struts/internal/util/directory"
	utilFile "gitlab.com/republico/util/dep_struts/internal/util/file"
	utilPath "gitlab.com/republico/util/dep_struts/internal/util/path"
)

var Deps model.DependenciesStruct
var separator string

func Load() error {
	separator = system.GetSeparator()

	pathProject := strings.TrimRight(Config.PathLoad, separator)
	if err := file.CreateDir(pathProject); err != nil {
		return err
	}

	if _, err := toml.DecodeFile(Config.PathFileConfig, &Deps); err != nil {
		return err
	}

	for _, d := range Deps.Dependencies {
		fmt.Println("\nDependency " + d.Name + ":")

		d.Url = "https://" + d.Name
		d.Path = utilPath.GetPathDependency(pathProject, d.Name, separator, d.Scope)

		flagExistsPath, _ := file.Exists(d.Path)

		if Config.Update && flagExistsPath {
			utilFile.Update(d)
		} else {
			if flagExistsPath {
				if err := os.RemoveAll(d.Path); err != nil {
					return err
				}
			}

			if err := utilFile.Download(d, Config.Git); err != nil {
				if err := directory.Remove(d.Path, separator); err != nil {
					return err
				}
			} else {
				flagExistsFileDep, _ := file.Exists(d.Path + separator + "Gopkg.toml")
				if flagExistsFileDep {
					importer.Config = importer.ConfigStruct{
						PathProject: d.Path,
						Git: true,
					}

					if err := importer.Import(); err != nil {
						exception.PrintError("    Error importing dependency " + d.Name + ".")
					}
				}
			}
		}
	}

	return nil
}