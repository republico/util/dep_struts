package importer

import (
	"fmt"
	"os"
	"strings"

	"github.com/BurntSushi/toml"

	"gitlab.com/republico/library/go/util/pkg/file"
	"gitlab.com/republico/library/go/util/pkg/system"

	"gitlab.com/republico/util/dep_struts/internal/model"
	"gitlab.com/republico/util/dep_struts/internal/util/directory"
	utilFile "gitlab.com/republico/util/dep_struts/internal/util/file"
	utilPath "gitlab.com/republico/util/dep_struts/internal/util/path"
)

var Deps model.DependenciesStruct
var separator string

func Import() error {
	separator = system.GetSeparator()

	pathProject := strings.TrimRight(Config.PathProject, separator)
	pathFileDep := pathProject + separator + "Gopkg.toml"

	flagExistsFileDep, _ := file.Exists(pathFileDep)
	if !flagExistsFileDep {
		fmt.Println("\nDependency does not contain the Gopkg.toml file.")
		return nil
	}

	if _, err := toml.DecodeFile(pathFileDep, &Deps); err != nil {
		return err
	}

	pathVendor := pathProject + "vendor"
	if err := generatePathVendor(pathVendor); err != nil {
		return err
	}

	for _, d := range Deps.Dependencies {
		fmt.Println("\nDependency " + d.Name + ":")

		d.Url = "https://" + d.Name
		d.Path = utilPath.GetPathDependency(pathVendor, d.Name, separator, d.Scope)

		if err := utilFile.Download(d, Config.Git); err != nil {
			if err := directory.Remove(d.Path, separator); err != nil {
				return err
			}
			return nil
		}
	}

	return nil
}

func generatePathVendor(path string) error {
	flag, _ := file.Exists(path)
	if flag {
		if err := os.RemoveAll(path); err != nil {
			return err
		}
	}

	if err := file.CreateDir(path); err != nil {
		return err
	}

	return nil
}