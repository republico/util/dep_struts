package file

import (
	"fmt"

	"gitlab.com/republico/library/go/util/pkg/cmd"
	"gitlab.com/republico/library/go/util/pkg/file"

	"gitlab.com/republico/util/dep_struts/internal/model"
)

var depUpdate model.DependencyStruct

func Update(dep model.DependencyStruct) error {
	depUpdate = dep

	if err := file.CreateDir(depUpdate.Path); err != nil {
		return err
	}

	fmt.Println("    Updating dependency.")

	_, err := cmd.Execute("git -C " + depUpdate.Path + " pull")
	if err != nil {
		fmt.Println("    An error has occurred in the update.")
		return err
	}
	
	fmt.Println("    Dependency updated.")

	return nil
}