package file

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/republico/library/go/util/pkg/cmd"
	"gitlab.com/republico/library/go/util/pkg/file"
	"gitlab.com/republico/library/go/util/pkg/system"

	"gitlab.com/republico/util/dep_struts/internal/model"
)

var depClone model.DependencyStruct
var separator string

func Download(dep model.DependencyStruct, git bool) error {
	depClone = dep

	separator = system.GetSeparator()

	if err := file.CreateDir(depClone.Path); err != nil {
		return err
	}

	fmt.Println("    Downloading dependency.")

	if depClone.Version != "" {
		if err := downloadZip(depClone.Version); err != nil {
			return err
		}
	} else if depClone.Branch != "" {
		if git {
			if err := cloneBranch(); err != nil {
				return err
			}
		} else {
			if err := downloadZip(depClone.Branch); err != nil {
				return err
			}
		}
	}
	
	fmt.Println("    Dependency downloaded.")

	return nil
}

func cloneBranch() error {
	_, err := cmd.Execute("git clone -b " + depClone.Branch + " " + depClone.Url + ".git " + depClone.Path)
	if err != nil {
		fmt.Println("    Dependency was not found.")
		return err
	}

	return nil
}

func downloadZip(brand string) error {
	urlZip := ""
	if strings.Contains(depClone.Name, "github.com") {
		urlZip = depClone.Url + "/archive/" + brand + ".zip"
	} else if strings.Contains(depClone.Name, "gitlab.com") {
		parts := strings.Split(depClone.Name, "/")
		urlZip = depClone.Url + "/-/archive/" + brand + "/" + parts[len(parts) - 1] + "-" + brand + ".zip"
	}

	tempFileZip := depClone.Path + separator + "temp.zip"

	if err := file.Download(urlZip, tempFileZip); err != nil {
		return err
	}

	if err := file.Unzip(tempFileZip, depClone.Path); err != nil {
		fmt.Println("    Dependency was not found.")
		if err := os.Remove(tempFileZip); err != nil {
			return nil
		}
		return nil
	}

	filesconstraint, err := ioutil.ReadDir(depClone.Path)
	if err != nil {
		return nil
	}

	for _, fileconstraint := range filesconstraint {
		tempDir := depClone.Path + separator + fileconstraint.Name()

		isDir, err := file.IsDirectory(tempDir)
		if err != nil {
			return nil
		}

		if isDir {
			filesUnzip, err := ioutil.ReadDir(tempDir)
			if err != nil {
				return nil
			}

			for _, fileUnzip := range filesUnzip {
				oldFile := tempDir + separator + fileUnzip.Name()
				newFile := depClone.Path + separator + fileUnzip.Name()

				err := os.Rename(oldFile, newFile)
				if err != nil {
					return err
				}
			}

			if err := os.Remove(tempDir); err != nil {
				return err
			}
		}

		flagExists, _ := file.Exists(tempFileZip)
		if flagExists {
			if err := os.Remove(tempFileZip); err != nil {
				return err
			}
		}
	}

	return nil
}