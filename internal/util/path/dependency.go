package path

import (
	"os"
	"strings"
)

func GetPathDependency(pathVendor string, name string, separator string, scope string) string {
	path := ""

	if scope == "global" {
		pathEnv := os.Getenv("GOPATH") + separator + "src"
		path = pathEnv + separator + name
	} else {
		path = pathVendor + separator + name
	}

	pathNormalized := strings.Replace(path, "/", separator, -1)

	return pathNormalized
}