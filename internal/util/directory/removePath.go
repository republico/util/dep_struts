package directory

import (
	"os"
	"strings"
)

func Remove(path string, separator string) error {
	pathRemove := path

	for {
		if err := os.Remove(pathRemove); err != nil {
			break
		}
	
		parts := strings.Split(pathRemove, separator)
	
		pathRemove = ""
		for _, p := range parts[:len(parts) - 1] {
			pathRemove += p + separator
		}
		pathRemove = strings.TrimRight(pathRemove, "/")
		
	}

	return nil
}