package main

import (
	"flag"
	"fmt"

	"gitlab.com/republico/library/go/util/pkg/exception"
	"gitlab.com/republico/util/dep_struts/internal/app/importer"
)

func main() {
	pathProject := flag.String("path", "./", "Endereço do projeto que terão as dependências importadas.")
	git := flag.Bool("git", false, "Utilizar comandos do git?")

	flag.Parse()

	importer.Config = importer.ConfigStruct{
		PathProject: *pathProject,
		Git: *git,
	}

	fmt.Println("\nImport started.")

	err := importer.Import()
	exception.HandlerError(err)

	fmt.Println("\nImport finished.")
}