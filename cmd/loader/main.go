package main

import (
	"flag"
	"fmt"

	"gitlab.com/republico/library/go/util/pkg/exception"
	"gitlab.com/republico/util/dep_struts/internal/app/loader"
)

func main() {
	pathFileConfig := flag.String("file", "", "Endereço do arquivo de configuração.")
	pathLoad := flag.String("path", "./", "Endereço de destino do carregamento.")
	git := flag.Bool("git", false, "Utilizar comandos do git?")
	update := flag.Bool("update", true, "Fazer a atualização das dependências existentes?")

	flag.Parse()

	loader.Config = loader.ConfigStruct{
		PathFileConfig: *pathFileConfig,
		PathLoad: *pathLoad,
		Git: *git,
		Update: *update,
	}

	if checkConfig(loader.Config) {
		fmt.Println("\nLoad started.")

		err := loader.Load()
		exception.HandlerError(err)

		fmt.Println("\nLoad finished.")
	} else {
		fmt.Println("\nInvalid or missing data.")
	}
}

func checkConfig(config loader.ConfigStruct) bool {
	result := true

	if len(config.PathFileConfig) == 0 {
		result = false
	}

	return result
}